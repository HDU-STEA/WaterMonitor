# WaterMonitor

#### 介绍
杭州电子科技大学2019年院“芯”苗项目客户端源码

#### 软件架构
基于Qt 5.12.5开发，主要代码使用QML和JavaScript，配置了Android Manifest以便生成APK文件，目录下有自动生成的iOS中间文件，在XCode中可以直接打开


#### 安装教程

1.  下载所有源码文件
2.  使用Qt Creator打开WaterMonitor.pro文件
3.  配置构建Kits即可

#### 使用说明

1.  此项目已适配Windows，iOS和安卓平台，构建时需要配合相应工具
2.  网络请求服务器代码暂时不开源，敬请谅解
