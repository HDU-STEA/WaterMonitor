import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Page {
	id: data_overview
	header: TabBar {
		id: data_overview_tab_bar
		currentIndex: data_overview_swipe_view.currentIndex
		Material.background: Material.Indigo
		Material.accent: "#ffffff"
		TabButton {
			text: qsTr("即时概览")
		}
		TabButton {
			text: qsTr("历史曲线")
		}
	}
	SwipeView {
		id: data_overview_swipe_view
		anchors.fill: parent
		currentIndex: data_overview_tab_bar.currentIndex

		OverviewList {}

		OverviewChart {}
	}
}

/*##^##
Designer {
	D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

