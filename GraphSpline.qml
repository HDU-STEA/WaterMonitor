import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Item {
	ListView {
		anchors.fill: parent

		model: Qt.fontFamilies()

		delegate: ItemDelegate {
			text: modelData
			width: parent.width
			onClicked: console.log("clicked:", modelData)
		}

		ScrollIndicator.vertical: ScrollIndicator {}
	}
}

/*##^##
Designer {
	D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

