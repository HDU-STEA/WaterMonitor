import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "Modules/functions.js" as Utils

Item {
	property alias autoUpdateTimer: auto_update_timer
	property alias currentDataDate: current_data_date
	property alias autoUpdateSwitch: auto_update_switch
	property alias currentDataCatagories: current_data_catagories
	property alias currentDataValues: current_data_values
	Timer {
		id: auto_update_timer
		interval: 6000
		repeat: true
		running: true
		triggeredOnStart: true
		onTriggered: Utils.getCurrentDatas()
	}
	ColumnLayout {
		anchors.rightMargin: 10
		anchors.leftMargin: 10
		anchors.fill: parent
		RowLayout {
			Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
			Layout.fillWidth: true
			Layout.fillHeight: false
			Text {
                text: qsTr("更新时间：")
                font.family: global_font
                verticalAlignment: Text.AlignVCenter
				horizontalAlignment: Text.AlignLeft
				Layout.fillWidth: false
			}
			Text {
				id: current_data_date
				wrapMode: Text.WordWrap
                font.family: global_font
				verticalAlignment: Text.AlignVCenter
				horizontalAlignment: Text.AlignLeft
				Layout.fillWidth: true
			}
			Text {
				text: qsTr("自动更新")
                font.family: global_font
				verticalAlignment: Text.AlignVCenter
				horizontalAlignment: Text.AlignRight
				Layout.fillWidth: true
			}
			Switch {
				id: auto_update_switch
				checked: true
				onToggled: Utils.setAutoUpdateTimer()
				Layout.fillWidth: false
			}
		}
		RowLayout {
			id: current_data_layout
			Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
			Layout.fillWidth: true
			Layout.fillHeight: true
			ListView {
				id: current_data_catagories
				Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
				Layout.fillWidth: true
				Layout.fillHeight: true

				boundsBehavior: Flickable.StopAtBounds

				model: ["TDS", "WaterBar", "Tss", "Bod", "PH", "Toc", "Cod", "Temperature", "NTU"]

				delegate: ItemDelegate {
					text: modelData
					width: parent.width
					onClicked: Utils.showCatagoryInfo(modelData)
				}
			}
			ListView {
				id: current_data_values
				Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
				Layout.fillWidth: true
				Layout.fillHeight: true

				boundsBehavior: Flickable.StopAtBounds

				model: ["0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0.0"]

				delegate: ItemDelegate {
					text: modelData
					width: parent.width
					//onClicked: Utils.getCurrentDatas()
				}
			}
		}
	}
}
/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

