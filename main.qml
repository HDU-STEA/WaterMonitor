import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12
import StatusBar 0.1

ApplicationWindow {
	readonly property string global_font: "PingFang TC"
	property alias toastItem: toast_displayer
	property alias main_window: main_window
	id: main_window
	visible: true
	//visibility: "FullScreen"
	width: 540
	height: 960
	title: qsTr("地表水参数监测系统")
	flags: Qt.Window | Qt.MaximizeUsingFullscreenGeometryHint
	header: ToolBar {
		z: 5
		topPadding: Qt.platform.os === "ios" ? Screen.height - Screen.desktopAvailableHeight : 0
		contentHeight: drawer_button.implicitHeight
		ColumnLayout {
			anchors.fill: parent
			Rectangle {
				visible: Qt.platform.os === "ios"
				Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
				Layout.fillWidth: true
				height: Screen.height - Screen.desktopAvailableHeight
			}
			RowLayout {
				Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
				Layout.fillHeight: true
				Layout.fillWidth: true
				ToolButton {
					id: drawer_button
					checkable: false
					down: false
					//icon.source: stackView.depth > 1 ? "Images/drawable-xxhdpi/baseline_arrow_back_ios_white_48.png" : "Images/drawable-xxhdpi/baseline_menu_white_48.png"


					/*
					onClicked: {
						if (stackView.depth > 1) {
							stackView.pop()
						} else {
							drawer.open()
						}
					}
					*/
				}
				Label {
					text: qsTr("地表水参数监测系统")
					verticalAlignment: Text.AlignVCenter
					horizontalAlignment: Text.AlignHCenter
					Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
					Layout.fillWidth: true
				}
				ToolButton {
					id: menu_button
					icon.source: "Images/drawable-xxhdpi/baseline_more_vert_white_48.png"
					onClicked: optionsMenu.open()
					Menu {
						id: optionsMenu
						x: parent.width - width
						transformOrigin: Menu.TopRight


						/*
						MenuItem {
							text: "Settings"
							onTriggered: settingsDialog.open()
						}
						*/
						MenuItem {
							text: qsTr("关于")
							onTriggered: aboutDialog.open()
						}
					}
				}
			}
		}
	}
	StatusBar {
		theme: "Dark"
		color: Material.color(Material.Indigo, Material.Shade500)
	}
	ToastManager {
		id: toast_displayer
	}
	Drawer {
		id: drawer
		width: menu_delegates.width + 10
		height: parent.height

		Column {
			anchors.fill: parent

			ItemDelegate {
				id: menu_delegates
				icon.source: "Images/drawable-xxhdpi/baseline_explore_white_48.png"
				text: qsTr("数据地图")
				onClicked: {
					stackView.push("DataMap.ui.qml")
					drawer.close()
				}
			}
		}
	}

	StackView {
		id: stackView
		z: 10
		initialItem: "DataOverview.ui.qml"
		anchors.fill: parent
	}


	/*
	Dialog {
		id: settingsDialog
		x: Math.round((main_window.width - width) / 2)
		y: Math.round(main_window.height / 6)
		width: Math.round(Math.min(main_window.width,
								   main_window.height) / 3 * 2)
		modal: true
		focus: true
		title: "Settings"

		standardButtons: Dialog.Ok | Dialog.Cancel
		onAccepted: {
			settings.style = styleBox.displayText
			settingsDialog.close()
		}
		onRejected: {
			styleBox.currentIndex = styleBox.styleIndex
			settingsDialog.close()
		}

		contentItem: ColumnLayout {
			id: settingsColumn
			spacing: 20

			RowLayout {
				spacing: 10

				Label {
					text: "Style:"
				}

				ComboBox {
					id: styleBox
					property int styleIndex: -1
					model: availableStyles
					Component.onCompleted: {
						styleIndex = find(settings.style, Qt.MatchFixedString)
						if (styleIndex !== -1)
							currentIndex = styleIndex
					}
					Layout.fillWidth: true
				}
			}

			Label {
				text: "Restart required"
				color: "#e41e25"
				opacity: styleBox.currentIndex !== styleBox.styleIndex ? 1.0 : 0.0
				horizontalAlignment: Label.AlignHCenter
				verticalAlignment: Label.AlignVCenter
				Layout.fillWidth: true
				Layout.fillHeight: true
			}
		}
	}
	*/
	Dialog {
		id: aboutDialog
		modal: true
		focus: true
		x: (main_window.width - width) / 2
		y: main_window.height / 6
		width: Math.min(main_window.width, main_window.height) / 3 * 2
		title: qsTr("关于")
		contentHeight: aboutColumn.height

		Column {
			id: aboutColumn
			spacing: 20

			Label {
				width: aboutDialog.availableWidth
				text: qsTr("本软件是电子信息学院院“芯”苗人才计划大学生科技创新资助项目“面向地表水参数数据显示与查阅的系统开发”所对应的用户客户端。")
				wrapMode: Label.Wrap
			}

			Label {
				width: aboutDialog.availableWidth
				text: qsTr("本应用可以实时获取当前监测点的水样数据，并提供参数历史数据曲线供用户查看。")
				wrapMode: Label.Wrap
			}
		}
	}
}
