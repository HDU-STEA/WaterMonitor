function setAutoUpdateTimer() {
	if (autoUpdateSwitch.position >= 0.5 && autoUpdateTimer.running === false) {
		autoUpdateTimer.running = true
		toastItem.show("自动更新已开启！")
	} else if (autoUpdateSwitch.position < 0.5
			   && autoUpdateTimer.running === true) {
		autoUpdateTimer.running = false
		toastItem.show("自动更新已关闭！")
	}
}
function updateEndModel() {
	var array = []
	var newDays = end_date_picker.end_days[end_month_tumbler.currentIndex]
	if (newDays === 28) {
		if(((end_year_tumbler.currentItem.text % 4 === 0)&&(end_year_tumbler.currentItem.text % 100 !== 0))||(end_year_tumbler.currentItem.text % 400 === 0)) {
			newDays = 29;
		}
	}
	for (var i = 1; i <= newDays; ++i)
		array.push(i)
	end_day_tumbler.model = array
}
function updateStartModel() {
	var array = []
	var newDays = start_date_picker.start_days[start_month_tumbler.currentIndex]
	if (newDays === 28) {
		if(((start_year_tumbler.currentItem.text % 4 === 0)&&(start_year_tumbler.currentItem.text % 100 !== 0))||(start_year_tumbler.currentItem.text % 400 === 0)) {
			newDays = 29;
		}
	}
	for (var i = 1; i <= newDays; ++i)
		array.push(i)
	start_day_tumbler.model = array
}
function getCurrentTime() {
	var currentDate = new Date()
	for (let i = 0; i < end_year_tumbler.model.length; i++) {
		if(currentDate.getFullYear() == end_year_tumbler.model[i]) {
			end_year_tumbler.currentIndex = i;
		}
	}
	for (let i = 0; i < end_month_tumbler.model.length; i++) {
		if((currentDate.getMonth() + 1) == end_month_tumbler.model[i]) {
			end_month_tumbler.currentIndex = i;
		}
	}
	updateEndModel();
	for (let i = 0; i < end_day_tumbler.model.length; i++) {
		if(currentDate.getDate() == end_day_tumbler.model[i]) {
			end_day_tumbler.currentIndex = i;
		}
	}
	for (let i = 0; i < end_hour_tumbler.model.length; i++) {
		if(currentDate.getHours() == end_hour_tumbler.model[i]) {
			end_hour_tumbler.currentIndex = i;
		}
	}
	for (let i = 0; i < end_minute_tumbler.model.length; i++) {
		if(currentDate.getMinutes() == end_minute_tumbler.model[i]) {
			end_minute_tumbler.currentIndex = i;
		}
	}
	for (let i = 0; i < end_second_tumbler.model.length; i++) {
		if(currentDate.getSeconds() == end_second_tumbler.model[i]) {
			end_second_tumbler.currentIndex = i;
		}
	}
	
}
function setCurrentTime() {
	start_year_tumbler.currentIndex = end_year_tumbler.currentIndex;
	start_month_tumbler.currentIndex = end_month_tumbler.currentIndex;
	updateStartModel();
	start_day_tumbler.currentIndex = end_day_tumbler.currentIndex;
	start_hour_tumbler.currentIndex = end_hour_tumbler.currentIndex;
	start_minute_tumbler.currentIndex = end_minute_tumbler.currentIndex;
	start_second_tumbler.currentIndex = end_second_tumbler.currentIndex;

}
function formatNumber(number) {
	return number < 10 && number >= 0 ? "0" + number : number.toString()
}
function getDefaultDatas(itemName) {

	open_end_time_dialog.enabled = false;
	get_default_history_data.visible = false;
	get_history_data_busy_indicator.visible = true;

	history_data_values.clear();
	var currentTime = new Date(), defaultTime = new Date();
	currentTime.setSeconds(currentTime.getSeconds() - 2);
	defaultTime.setHours(currentTime.getHours() - 4);
	var currentDate = currentTime.getFullYear().toString() + "-";
	currentDate += ((currentTime.getMonth() + 1 >= 10) ? (currentTime.getMonth() + 1).toString() : "0" + (currentTime.getMonth() + 1).toString()) + "-";
	currentDate += (currentTime.getDate() >= 10 ? currentTime.getDate().toString() : "0" + currentTime.getDate().toString()) + "T";
	currentDate += (currentTime.getHours() >= 10 ? currentTime.getHours().toString() : "0" + currentTime.getHours().toString()) + ":";
	currentDate += (currentTime.getMinutes() >= 10 ? currentTime.getMinutes().toString() : "0" + currentTime.getMinutes().toString()) + ":";
	currentDate += currentTime.getSeconds() >= 10 ? currentTime.getSeconds().toString() : "0" + currentTime.getSeconds().toString();
	console.log(currentDate);
	var defaultDate = defaultTime.getFullYear().toString() + "-";
	defaultDate += ((defaultTime.getMonth() + 1 >= 10) ? (defaultTime.getMonth() + 1).toString() : "0" + (defaultTime.getMonth() + 1).toString()) + "-";
	defaultDate += (defaultTime.getDate() >= 10 ? defaultTime.getDate().toString() : "0" + defaultTime.getDate().toString()) + "T";
	defaultDate += (defaultTime.getHours() >= 10 ? defaultTime.getHours().toString() : "0" + defaultTime.getHours().toString()) + ":";
	defaultDate += (defaultTime.getMinutes() >= 10 ? defaultTime.getMinutes().toString() : "0" + defaultTime.getMinutes().toString()) + ":";
	defaultDate += defaultTime.getSeconds() >= 10 ? defaultTime.getSeconds().toString() : "0" + defaultTime.getSeconds().toString();
	console.log(defaultDate);

	history_data_view_x_axis.min = defaultTime;
	history_data_view_x_axis.max = currentTime;	
	history_data_view_x_axis.format = "hh:mm";

	var intervalTime = "460";
	var xmlhttp = new XMLHttpRequest()
	xmlhttp.open("GET",
				 "http://106.14.117.40/api/getHistoryData.php?startTime=" + defaultDate + "&endTime="
				 + currentDate + "&interval=" + intervalTime + "&item=" + itemName + "&forceInterval=true",
				 true)
	xmlhttp.onreadystatechange = function () {
		if (xmlhttp.readyState === 4) {
			if (xmlhttp.status === 200) {
				var response = JSON.parse(xmlhttp.response)
				if (response.code === 1) {
					if (response.message !== "") {
						toastItem.show(response.message)
					}
					if (response.data.length !== 0) {
						var minValue = response.data[0].y, maxValue = response.data[0].y;
					} else {
						var minValue = -500, maxValue = -404;
					}
					console.log(response);
					for (var i = 0; i < response.data.length; i++) {
						if (response.data[i].y < minValue) {
							minValue = response.data[i].y;
						} else if (response.data[i].y > maxValue) {
							maxValue = response.data[i].y;
						}
						history_data_values.append(
									(Date.fromLocaleString(
										 Qt.locale(), response.data[i].x,
										 "yyyy-MM-dd hh:mm:ss.zzz")).getTime(),
									response.data[i].y)
					}
					history_data_view_y_axis.min = minValue - (maxValue - minValue)/8;
					history_data_view_y_axis.max = maxValue + (maxValue - minValue)/8;

					open_end_time_dialog.enabled = true;
					get_default_history_data.visible = true;
					get_history_data_busy_indicator.visible = false;

				} else {
					toastItem.show("返回错误！\n" + response.message);

					open_end_time_dialog.enabled = true;
					get_default_history_data.visible = true;
					get_history_data_busy_indicator.visible = false;
				}
			} else {
				toastItem.show("数据获取失败！请检查网络连接设置");

				open_end_time_dialog.enabled = true;
				get_default_history_data.visible = true;
				get_history_data_busy_indicator.visible = false;
			}
		}
	}
	xmlhttp.send()
}
function getHistoryDatas(itemName) {

	open_end_time_dialog.visible = false;
	get_default_history_data.enabled = false;
	get_history_data_busy_indicator.visible = true;

	history_data_values.clear();
	var startTime = start_year_tumbler.currentItem.text + "-" + start_month_tumbler.currentItem.text + "-" + start_day_tumbler.currentItem.text + "T" + start_hour_tumbler.currentItem.text + ":" + start_minute_tumbler.currentItem.text + ":" + start_second_tumbler.currentItem.text;
	var endTime = end_year_tumbler.currentItem.text + "-" + end_month_tumbler.currentItem.text + "-" + end_day_tumbler.currentItem.text + "T" + end_hour_tumbler.currentItem.text + ":" + end_minute_tumbler.currentItem.text + ":" + end_second_tumbler.currentItem.text;

	var startDate = Date.fromLocaleString(Qt.locale(), startTime, "yyyy-MM-ddThh:mm:ss");
	var endDate = Date.fromLocaleString(Qt.locale(), endTime, "yyyy-MM-ddThh:mm:ss");
	history_data_view_x_axis.min = startDate;
	history_data_view_x_axis.max = endDate;	
	var deltaTime = (endDate.getTime() - startDate.getTime())/1000;
	console.log("Time gap:" + deltaTime.toString() + "s");
	var intervalTime = deltaTime/20;
	console.log("Time Interval: " + intervalTime.toString() + "s");

	if(deltaTime >= 63072000){
		history_data_view_x_axis.format = "yyyy";
	} else if(deltaTime < 63072000 && deltaTime >= 7776000){
		history_data_view_x_axis.format = "yyyy-mm";
	} else if(deltaTime < 7776000 && deltaTime >= 345600){
		history_data_view_x_axis.format = "mm-dd";
	} else if(deltaTime < 345600 && deltaTime >= 14400){
		history_data_view_x_axis.format = "ddThh";
	} else if(deltaTime < 14400 && deltaTime >= 600){
		history_data_view_x_axis.format = "hh:mm";
	} else if(deltaTime < 600 && deltaTime >= 60) {
		history_data_view_x_axis.format = "hh:mm:ss";
	} else if(deltaTime < 60 && deltaTime >= 0) {
		history_data_view_x_axis.format = "mm:ss.zzz";
	}

	var xmlhttp = new XMLHttpRequest()
	xmlhttp.open("GET",
				 "http://106.14.117.40/api/getHistoryData.php?startTime=" + startTime + "&endTime="
				 + endTime + "&interval=" + intervalTime + "&item=" + itemName + "&forceInterval=true",
				 true)
	xmlhttp.onreadystatechange = function () {
		if (xmlhttp.readyState === 4) {
			if (xmlhttp.status === 200) {
				var response = JSON.parse(xmlhttp.response)
				if (response.code === 1) {
					if (response.message !== "") {
						toastItem.show(response.message)
					}
					if (response.data.length !== 0) {
						var minValue = response.data[0].y, maxValue = response.data[0].y;
					} else {
						var minValue = -500, maxValue = -404;
					}
					for (var i = 0; i < response.data.length; i++) {
						if (response.data[i].y < minValue) {
							minValue = response.data[i].y;
						} else if (response.data[i].y > maxValue) {
							maxValue = response.data[i].y;
						}
						console.log(response.data[i].y);
						history_data_values.append(
									(Date.fromLocaleString(
										 Qt.locale(), response.data[i].x,
										 "yyyy-MM-dd hh:mm:ss.zzz")).getTime(),
									response.data[i].y)
					}
					history_data_view_y_axis.min = minValue - (maxValue - minValue)/8;
					history_data_view_y_axis.max = maxValue + (maxValue - minValue)/8;

					open_end_time_dialog.visible = true;
					get_default_history_data.enabled = true;
					get_history_data_busy_indicator.visible = false;
				} else {
					toastItem.show("返回错误！\n" + response.message)

					open_end_time_dialog.visible = true;
					get_default_history_data.enabled = true;
					get_history_data_busy_indicator.visible = false;
				}
			} else {
				toastItem.show("数据获取失败！请检查网络连接设置")

				open_end_time_dialog.visible = true;
				get_default_history_data.enabled = true;
				get_history_data_busy_indicator.visible = false;
			}
		}
	}
	xmlhttp.send()
}
function getCurrentDatas() {
	var xmlhttp2 = new XMLHttpRequest();
	xmlhttp2.open("GET", "http://106.14.117.40/api/getDatastreams.php", true);
	xmlhttp2.onreadystatechange = function () {
		if (xmlhttp2.readyState === 4) {
			var returnValue = Array();
			if (xmlhttp2.status === 200) {
				var response = JSON.parse(xmlhttp2.response);
				if (response.errno === 0) {
					currentDataDate.text = response.data.datastreams[0].datapoints[0].at.substring(0,10);
					currentDataDate.text += "\n" + response.data.datastreams[0].datapoints[0].at.substring(11,19)
					for (var i = 0; i < 9; i++) {
						returnValue[i] = response.data.datastreams[i].datapoints[0].value;
					}
					currentDataValues.model = returnValue;
				} else {
					currentDataDate.text = "Null"
					for (var i = 0; i < 9; i++) {
						returnValue[i] = "Null";
					}
					currentDataValues.model = returnValue;
					toastItem.show("数据获取失败，状态码：" + response.errno);
				}
			} else {
				currentDataDate.text = "Null"
				for (var i = 0; i < 9; i++) {
					returnValue[i] = "Null";
				}
				currentDataValues.model = returnValue;
				toastItem.show("网络请求失败，状态码：" + xmlhttp2.status);
			}
		} 
	}
	xmlhttp2.send();
}