import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Page {
	id: data_graph
	opacity: 0.25
	header: TabBar {
		id: data_graph_tab_bar
		currentIndex: data_graph_swipe_view.currentIndex
		Material.background: Material.Indigo
		Material.accent: "#ffffff"
		TabButton {
			text: qsTr("字体列表")
		}
		TabButton {
			text: qsTr("未完成")
		}
	}
	SwipeView {
		id: data_graph_swipe_view
		anchors.fill: parent
		currentIndex: data_graph_tab_bar.currentIndex

		GraphSpline {}

		GraphMap {}
	}
}

/*##^##
Designer {
	D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

