import QtQuick 2.12
import QtQuick.Controls 2.12
import QtCharts 2.3
import QtQuick.Layouts 1.12
import "Modules/functions.js" as Utils

Item {
    property alias category_combo_box: category_combo_box
    property alias get_default_history_data: get_default_history_data
    property alias get_history_data_busy_indicator: get_history_data_busy_indicator
    RowLayout {
        id: history_selector_bar
        spacing: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.bottom: history_data_view.top
        anchors.bottomMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10

        z: 5

        Text {
            text: qsTr("数据类型：")
            font.family: global_font
            wrapMode: Text.WordWrap
        }
        ComboBox {
            id: category_combo_box
            spacing: 0
            flat: true
            model: ["TDS", "WaterBar", "Tss", "Bod", "PH", "Toc", "Cod", "Temperature", "NTU"]
        }
        Button {
            visible: true
            id: open_end_time_dialog
            text: qsTr("设置区间")
            spacing: 0
            highlighted: true
            onClicked: {
                Utils.getCurrentTime()
                set_end_time_dialog.open()
            }
        }
        BusyIndicator {
            visible: false
            id: get_history_data_busy_indicator
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.fillHeight: true
            spacing: 0
        }
        Button {
            visible: true
            id: get_default_history_data
            text: qsTr("自动获取")
            spacing: 0
            highlighted: true
            onClicked: Utils.getDefaultDatas(category_combo_box.currentText)
        }
    }

    ChartView {
        property alias history_data_view: history_data_view
        property alias history_data_values: history_data_values
        property alias history_data_view_x_axis: history_data_view_x_axis
        property alias history_data_view_y_axis: history_data_view_y_axis

        id: history_data_view
        backgroundColor: "#00ffffff"
        anchors.top: history_selector_bar.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.topMargin: 20

        antialiasing: true
        z: -5

        legend.visible: false
        animationDuration: 1500
        animationOptions: ChartView.NoAnimation
        dropShadowEnabled: true
        LineSeries {
            id: history_data_values
            pointsVisible: true
            pointLabelsVisible: true
            pointLabelsFormat: "@yPoint"
            axisY: ValueAxis {
                id: history_data_view_y_axis
                min: 0.0
                max: 100.0
            }
            axisX: DateTimeAxis {
                id: history_data_view_x_axis
                format: "hh:mm" //设置显示样式
                min: Date.fromLocaleString(Qt.locale(), "2018-12-31T00:00:00",
                                           "yyyy-MM-ddThh:mm:ss")
                max: Date.fromLocaleString(Qt.locale(), "2019-01-01T00:00:00",
                                           "yyyy-MM-ddThh:mm:ss")
            }
        }
    }
    Dialog {
        id: set_start_time_dialog
        x: Math.round((main_window.width - width) / 2)
        y: Math.round(
               (main_window.height - height) / 2 - (main_window.height - parent.height))
        width: Math.round(main_window.width / 5 * 4)
        height: Math.round(main_window.height / 5 * 4)
        modal: true
        focus: true

        //title: qsTr("设置开始时间")
        footer: DialogButtonBox {
            Button {
                text: qsTr("确认")
                highlighted: true
                flat: false
                DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
            }
            Button {
                text: qsTr("取消")
                highlighted: true
                flat: true
                DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
            }
        }
        onAccepted: {
            Utils.getHistoryDatas(category_combo_box.currentText)
            set_start_time_dialog.close()
        }
        onRejected: {
            set_start_time_dialog.close()
        }

        contentItem: ColumnLayout {
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            anchors.fill: parent
            Text {
                text: qsTr("设置开始日期")
                font.family: global_font

                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: false
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }
            RowLayout {
                id: start_date_picker

                property alias start_year_tumbler: start_year_tumbler
                property alias start_month_tumbler: start_month_tumbler
                property alias start_day_tumbler: start_day_tumbler

                readonly property var start_days: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
                Layout.topMargin: 20
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                Tumbler {
                    id: start_year_tumbler
                    // This array is populated with the previous three years. For example: [2017, 2018, 2019]
                    readonly property var start_years: (function () {
                        var currentYear = new Date().getFullYear()
                        return [-5, -4, -3, -2, -1, 0].map(function (value) {
                            return value + currentYear
                        })
                    })()
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    wrap: false
                    onCurrentIndexChanged: Utils.updateStartModel()
                    model: start_years
                    delegate: TumblerDelegate {
                        text: Utils.formatNumber(modelData)
                    }
                }
                Text {
                    text: qsTr("年")
                    font.family: global_font

                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                Tumbler {
                    id: start_month_tumbler
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    onCurrentIndexChanged: Utils.updateStartModel()

                    model: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                    delegate: TumblerDelegate {
                        text: Utils.formatNumber(modelData)
                    }
                }
                Text {
                    text: qsTr("月")
                    font.family: global_font

                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                Tumbler {
                    id: start_day_tumbler
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    delegate: TumblerDelegate {
                        text: Utils.formatNumber(modelData)
                    }
                }
                Text {
                    text: qsTr("日")
                    font.family: global_font

                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
            }
            Text {
                text: qsTr("设置开始时间")
                font.family: global_font

                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: false
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }
            RowLayout {
                id: start_time_picker

                property alias start_hour_tumbler: start_hour_tumbler
                property alias start_minute_tumbler: start_minute_tumbler
                property alias start_second_tumbler: start_second_tumbler
                Layout.bottomMargin: 20
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                Tumbler {
                    id: start_hour_tumbler
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    model: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]

                    delegate: TumblerDelegate {
                        text: Utils.formatNumber(modelData)
                    }
                }
                Text {
                    text: qsTr("时")
                    font.family: global_font

                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                Tumbler {
                    id: start_minute_tumbler
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    model: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59]

                    delegate: TumblerDelegate {
                        text: Utils.formatNumber(modelData)
                    }
                }
                Text {
                    text: qsTr("分")
                    font.family: global_font

                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                Tumbler {
                    id: start_second_tumbler
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    model: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59]

                    delegate: TumblerDelegate {
                        text: Utils.formatNumber(modelData)
                    }
                }
                Text {
                    text: qsTr("秒")
                    font.family: global_font

                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
            }
        }
    }
    Dialog {
        id: set_end_time_dialog
        x: Math.round((main_window.width - width) / 2)
        y: Math.round(
               (main_window.height - height) / 2 - (main_window.height - parent.height))
        width: Math.round(main_window.width / 5 * 4)
        height: Math.round(main_window.height / 5 * 4)
        modal: true
        focus: true

        //title: qsTr("设置截止时间")
        footer: DialogButtonBox {
            Button {
                text: qsTr("确认")
                highlighted: true
                flat: false
                DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
            }
            Button {
                text: qsTr("取消")
                highlighted: true
                flat: true
                DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
            }
        }
        onAccepted: {
            Utils.setCurrentTime()
            set_end_time_dialog.close()
            set_start_time_dialog.open()
        }
        onRejected: {
            set_end_time_dialog.close()
        }

        contentItem: ColumnLayout {
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            anchors.fill: parent
            Text {
                text: qsTr("设置截止日期")
                font.family: global_font

                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: false
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }
            RowLayout {
                id: end_date_picker

                property alias end_year_tumbler: end_year_tumbler
                property alias end_month_tumbler: end_month_tumbler
                property alias end_day_tumbler: end_day_tumbler

                readonly property var end_days: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
                Layout.topMargin: 0
                Layout.bottomMargin: -10
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                Tumbler {
                    id: end_year_tumbler
                    // This array is populated with the previous three years. For example: [2017, 2018, 2019]
                    readonly property var end_years: (function () {
                        var currentYear = new Date().getFullYear()
                        return [-5, -4, -3, -2, -1, 0].map(function (value) {
                            return value + currentYear
                        })
                    })()
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    wrap: false
                    onCurrentIndexChanged: Utils.updateEndModel()
                    model: end_years
                    delegate: TumblerDelegate {
                        text: Utils.formatNumber(modelData)
                    }
                }
                Text {
                    text: qsTr("年")
                    font.family: global_font

                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                Tumbler {
                    id: end_month_tumbler
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    onCurrentIndexChanged: Utils.updateEndModel()

                    model: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                    delegate: TumblerDelegate {
                        text: Utils.formatNumber(modelData)
                    }
                }
                Text {
                    text: qsTr("月")
                    font.family: global_font

                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                Tumbler {
                    id: end_day_tumbler
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    delegate: TumblerDelegate {
                        text: Utils.formatNumber(modelData)
                    }
                }
                Text {
                    text: qsTr("日")
                    font.family: global_font

                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
            }
            Text {
                text: qsTr("设置截止时间")
                font.family: global_font
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: false
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }
            RowLayout {
                id: end_time_picker
                property alias end_hour_tumbler: end_hour_tumbler
                property alias end_minute_tumbler: end_minute_tumbler
                property alias end_second_tumbler: end_second_tumbler
                Layout.topMargin: -10
                Layout.bottomMargin: 20
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                Tumbler {
                    id: end_hour_tumbler
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    model: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]

                    delegate: TumblerDelegate {
                        text: Utils.formatNumber(modelData)
                    }
                }
                Text {
                    text: qsTr("时")
                    font.family: global_font

                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                Tumbler {
                    id: end_minute_tumbler
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    model: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59]

                    delegate: TumblerDelegate {
                        text: Utils.formatNumber(modelData)
                    }
                }
                Text {
                    text: qsTr("分")
                    font.family: global_font

                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                Tumbler {
                    id: end_second_tumbler
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    model: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59]

                    delegate: TumblerDelegate {
                        text: Utils.formatNumber(modelData)
                    }
                }
                Text {
                    text: qsTr("秒")
                    font.family: global_font

                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: false
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
            }
        }
    }
}
/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

